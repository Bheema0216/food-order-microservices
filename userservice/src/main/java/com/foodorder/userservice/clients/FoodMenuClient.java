package com.foodorder.userservice.clients;

import java.util.ArrayList;
import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.foodorder.userservice.constants.UserConstants;
import com.foodorder.userservice.dtos.FoodCategory;
import com.foodorder.userservice.dtos.FoodItemsResponse;
import com.foodorder.userservice.exceptions.FoodItemNotFoundException;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;

@FeignClient("FOODMENU-SERVICE")
@CircuitBreaker(name = "foodcircuitbreaker", fallbackMethod = "findFoodItemByNameFallBackMethod")
public interface FoodMenuClient {

	@GetMapping("/foodmenus/bynamelike")
	public List<FoodItemsResponse> findFoodItemByNameLike(@RequestParam String foodName, @RequestParam int page,
			@RequestParam int size) throws FoodItemNotFoundException;

	public default List<FoodItemsResponse> findFoodItemByNameFallBackMethod(Exception ex) throws FoodItemNotFoundException {
		List<FoodItemsResponse> response = new ArrayList<FoodItemsResponse>();
		System.out.println("fallback-method for fooedmenu service:"+ex.getMessage());
		if(ex.getMessage().contains("No Food items")) {
			throw new FoodItemNotFoundException(UserConstants.FOOD_ITEM_NOT_FOUND);
		}
		response.add(new FoodItemsResponse("Bun", "soft", FoodCategory.VEG, 10.00));
		return response;
	}

}
