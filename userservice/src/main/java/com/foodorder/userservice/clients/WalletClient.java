package com.foodorder.userservice.clients;

import javax.validation.Valid;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.foodorder.userservice.constants.UserConstants;
import com.foodorder.userservice.dtos.WalletRequestDto;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;

@FeignClient("WALLET-SERVICE")
@CircuitBreaker(name = "walletcircuitbreaker", fallbackMethod = "addAmountToWalletFallBackMethod")
public interface WalletClient {

	@PutMapping("/wallets")
	public String addAmountToWallet(@Valid @RequestBody WalletRequestDto walletRequest);
	
	@PostMapping("/wallets")
	public String createWallet(@Valid @RequestBody WalletRequestDto walletRequest);

	public default String addAmountToWalletFallBackMethod(Exception ex) {
		return UserConstants.WALLET_SERVICE_FALLBACK_METHOD_RESPONSE;
	}

}
