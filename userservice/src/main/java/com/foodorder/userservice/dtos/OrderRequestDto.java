package com.foodorder.userservice.dtos;

import java.util.List;


import lombok.Data;

@Data
public class OrderRequestDto {
	private Integer userId;

	List<OrderDetailsRequestDto> orderDetailsRequestDto;
}
