package com.foodorder.userservice.dtos;

import java.time.LocalDate;

import lombok.Data;

@Data
public class OrderResponse {
	private Integer orderId;

	private LocalDate orderedDate;

	private double totalPrice;
	
	private double availableWalletBalence;
}
