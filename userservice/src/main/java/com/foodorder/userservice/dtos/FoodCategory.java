package com.foodorder.userservice.dtos;

import lombok.Getter;

@Getter
public enum FoodCategory {

	VEG, NON_VEG;
}
