package com.foodorder.userservice.dtos;

import lombok.Data;

@Data
public class UserResponseDto {
	private Integer userId;

	private String userName;

	private String userPhoneNumber;

	private String email;
}
