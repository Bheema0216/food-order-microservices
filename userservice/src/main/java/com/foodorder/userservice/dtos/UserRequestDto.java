package com.foodorder.userservice.dtos;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.foodorder.userservice.entity.Address;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserRequestDto {
	
	@NotNull
	@Size(max = 40)
	private String userName;

	@Size(min = 10, max = 10, message = "PhoneNumber must be eqaul to 10 numbers")
	private String userPhoneNumber;

	@Email
	private String email;
	
	@Size(min = 4, max = 10, message = "Password length should be 4 to 10 charecters")
	private String password;
	
	private boolean active;
	
	private String roles;
	
	private Address address;
}
