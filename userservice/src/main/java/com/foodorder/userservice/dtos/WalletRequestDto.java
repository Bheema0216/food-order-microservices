package com.foodorder.userservice.dtos;

import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class WalletRequestDto {
	@NotNull
	private Integer userId;
	@NotNull
	private double balance;
}
