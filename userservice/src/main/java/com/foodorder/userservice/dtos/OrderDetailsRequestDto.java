package com.foodorder.userservice.dtos;

import lombok.Data;

@Data
public class OrderDetailsRequestDto {
	private String foodName;

	private int quntity;
}
