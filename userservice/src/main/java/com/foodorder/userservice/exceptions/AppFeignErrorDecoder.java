package com.foodorder.userservice.exceptions;

import org.springframework.stereotype.Component;

import com.foodorder.userservice.constants.UserConstants;

import feign.Response;
import feign.codec.ErrorDecoder;

@Component
public class AppFeignErrorDecoder implements ErrorDecoder {

	private final ErrorDecoder defaultErrorDecoder = new Default();

	@Override
	public Exception decode(String methodKey, Response response) {
		if (response.status() == 404) {
			throw new FoodItemNotFoundException(UserConstants.FOOD_ITEM_NOT_FOUND);
		}

		return defaultErrorDecoder.decode(methodKey, response);
	}

}