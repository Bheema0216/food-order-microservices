package com.foodorder.userservice.exceptions;

public class UserAlreadyExistExcpetion extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public UserAlreadyExistExcpetion(String msg) {
		super(msg);
	}
}
