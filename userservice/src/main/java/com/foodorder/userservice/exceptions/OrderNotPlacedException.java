package com.foodorder.userservice.exceptions;

public class OrderNotPlacedException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public OrderNotPlacedException(String msg) {
		super(msg);
	}

}
