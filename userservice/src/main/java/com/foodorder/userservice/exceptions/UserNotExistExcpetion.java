package com.foodorder.userservice.exceptions;

public class UserNotExistExcpetion extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UserNotExistExcpetion(String msg) {
		super(msg);
	}

}
