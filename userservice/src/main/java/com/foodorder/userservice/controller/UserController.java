package com.foodorder.userservice.controller;

import java.security.Principal;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.foodorder.userservice.constants.UserConstants;
import com.foodorder.userservice.dtos.FoodItemsResponse;
import com.foodorder.userservice.dtos.UserRequestDto;
import com.foodorder.userservice.dtos.UserResponseDto;
import com.foodorder.userservice.dtos.WalletRequestDto;
import com.foodorder.userservice.exceptions.FoodItemNotFoundException;
import com.foodorder.userservice.service.UserService;

@RestController
@Validated
public class UserController {

	@Autowired
	UserService userService;

	@PostMapping("/users")
	public ResponseEntity<UserResponseDto> addUser(@Valid @RequestBody UserRequestDto userRequestDto) {

		UserResponseDto response = userService.addUser(userRequestDto);
		return new ResponseEntity<>(response, HttpStatus.CREATED);
	}

	@GetMapping("/users/login")
	@PreAuthorize("hasAuthority('ROLE_USER') or hasAuthority('ROLE_MODERATOR')")
	public ResponseEntity<String> login(Principal principal) {
		userService.loadUserByUsername(principal.getName());
		return new ResponseEntity<>("User logged in successfully", HttpStatus.ACCEPTED);
	}

	@GetMapping("/users/access/{userId}/{userRole}")
	@PreAuthorize("hasAuthority('ROLE_ADMIN') or hasAuthority('ROLE_MODERATOR')")
	public ResponseEntity<String> giveAcessToUser(@PathVariable int userId, @PathVariable String userRole,
			Principal principal) {
		String response = userService.findById(userId, userRole, principal);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@GetMapping("/users/foodmenus")
	public ResponseEntity<List<FoodItemsResponse>> findFoodItemByName(@NotNull @RequestParam String foodName,
			@RequestParam int page, @RequestParam int size) throws FoodItemNotFoundException {
		List<FoodItemsResponse> response = userService.findFoodItemByName(foodName, page, size);
		if(response.isEmpty()) throw new FoodItemNotFoundException(UserConstants.FOOD_ITEM_NOT_FOUND+foodName); 
		
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@PutMapping("/users/wallets")
	public ResponseEntity<String> addAmountToUserWallet(@Valid @RequestBody WalletRequestDto walletResquest) {
		String response = userService.addAmountToUserWallet(walletResquest);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	/*
	 * @PostMapping("/users/orders") public ResponseEntity<OrderResponse>
	 * placeOrder(@RequestBody OrderRequestDto orderRequestDto) throws
	 * OrderNotPlacedException, FoodItemNotFoundException{ OrderResponse
	 * orderResponse = userService.placeOrder(orderRequestDto); return new
	 * ResponseEntity<>(orderResponse, HttpStatus.OK); }
	 */

}
