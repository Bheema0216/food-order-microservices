package com.foodorder.userservice.serviceimpl;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.foodorder.userservice.clients.FoodMenuClient;
import com.foodorder.userservice.clients.WalletClient;
import com.foodorder.userservice.constants.UserConstants;
import com.foodorder.userservice.dtos.FoodItemsResponse;
import com.foodorder.userservice.dtos.UserRequestDto;
import com.foodorder.userservice.dtos.UserResponseDto;
import com.foodorder.userservice.dtos.WalletRequestDto;
import com.foodorder.userservice.entity.User;
import com.foodorder.userservice.exceptions.FoodItemNotFoundException;
import com.foodorder.userservice.exceptions.UserAlreadyExistExcpetion;
import com.foodorder.userservice.exceptions.UserNotExistExcpetion;
import com.foodorder.userservice.repository.UserRepository;
import com.foodorder.userservice.service.UserService;

@Service
@Transactional
public class UserServiceImpl implements UserService {

	@Autowired
	UserRepository userRepository;

	@Autowired
	private BCryptPasswordEncoder passwordEncoder;

	@Autowired
	private FoodMenuClient foodMenuClient;

	@Autowired
	private WalletClient walletClient;

	@Override
	public UserResponseDto addUser(@Valid UserRequestDto userRequestDto) {

		Optional<User> existedUser = userRepository.findByEmailOrUserPhoneNumber(userRequestDto.getUserName(),
				userRequestDto.getUserPhoneNumber());

		if (existedUser.isPresent())
			throw new UserAlreadyExistExcpetion(UserConstants.USER_IS_ALREADY_EXISTED_EXCEPTION_MESSAGE);

		userRequestDto.setRoles(UserConstants.DEFAULT_ROLE);
		String encryptedPwd = passwordEncoder.encode(userRequestDto.getPassword());
		userRequestDto.setPassword(encryptedPwd);

		User user = new User();

		user.setUserAddress(userRequestDto.getAddress());
		BeanUtils.copyProperties(userRequestDto, user);
		User savedUser = userRepository.save(user);

		UserResponseDto response = new UserResponseDto();
		BeanUtils.copyProperties(savedUser, response);

		WalletRequestDto walletRequest = new WalletRequestDto();
		walletRequest.setUserId(savedUser.getUserId());
		walletClient.createWallet(walletRequest);
		return response;
	}

	@Override
	public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
		Optional<User> user = userRepository.findByUserName(userName);
		return user.map(UserDetailsImpl::new)
				.orElseThrow(() -> new UsernameNotFoundException(userName + " not found."));
	}

	@Override
	public String findById(int userId, String userRole, Principal principal) {

		User user = userRepository.findById(userId).get();

		List<String> activeRoles = getRolesByLoggedInUser(principal);
		String newRole = "";
		if (activeRoles.contains(userRole)) {
			newRole = user.getRoles() + "," + userRole;
			user.setRoles(newRole);
		}
		userRepository.save(user);
		return "Hi " + user.getUserName() + " New role got assigned to you by " + principal.getName();
	}

	private List<String> getRolesByLoggedInUser(Principal principal) {

		String roles = getLoggedInUser(principal).getRoles();
		List<String> assignRoles = Arrays.stream(roles.split(",")).collect(Collectors.toList());

		if (assignRoles.contains("ROLE_ADMIN")) {
			return Arrays.stream(UserConstants.ADMIN_ACCESS).collect(Collectors.toList());
		}
		if (assignRoles.contains("ROLE_MODERATOR")) {
			return Arrays.stream(UserConstants.MODERATOR_ACCESS).collect(Collectors.toList());
		}
		return Collections.emptyList();
	}

	private User getLoggedInUser(Principal principal) {
		return userRepository.findByUserName(principal.getName()).get();
	}

	@Override
	public List<FoodItemsResponse> findFoodItemByName(@NotNull String foodName, int page, int size)
			throws FoodItemNotFoundException {
		try {
			System.out.println("user-service-impl");
			return foodMenuClient.findFoodItemByNameLike(foodName, page, size);
		} catch (Exception e) {
			e.getMessage();
		}
		List<FoodItemsResponse> response = new ArrayList<FoodItemsResponse>();
		return response;
	}

	@Override
	public String addAmountToUserWallet(@Valid WalletRequestDto walletRequest) {
		Optional<User> isUserExists = userRepository.findById(walletRequest.getUserId());
		if (!isUserExists.isPresent())
			throw new UserNotExistExcpetion(UserConstants.USER_NOT_FOUND + walletRequest.getUserId());
		return walletClient.addAmountToWallet(walletRequest);
	}

	/*
	 * @Override public OrderResponse placeOrder(OrderRequestDto
	 * orderRequestDto)throws OrderNotPlacedException, FoodItemNotFoundException {
	 * Optional<User> isUserExists =
	 * userRepository.findById(orderRequestDto.getUserId()); if
	 * (!isUserExists.isPresent()) throw new
	 * UserNotExistExcpetion(UserConstants.USER_NOT_FOUND +
	 * orderRequestDto.getUserId()); return orderClient.placeOrder(orderRequestDto);
	 * }
	 */

}
