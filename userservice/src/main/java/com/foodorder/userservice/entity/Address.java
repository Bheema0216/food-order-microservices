package com.foodorder.userservice.entity;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Data;

@Data
@Embeddable
public class Address {
	
	@NotNull
	@Size(max = 40)
	private String addressLine1;
	@NotNull
	@Size(max = 40)
	private String addressLine2;
	@NotNull
	@Size(max = 20)
	private String city;
	@NotNull
	@Size(max = 20)
	private String state;
	@NotNull
	@Size(max = 20)
	private String country;
	@NotNull
	@Size(max = 7)
	private String zipCode;
}
