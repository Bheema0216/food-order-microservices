package com.foodorder.userservice.service;

import java.security.Principal;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.foodorder.userservice.dtos.FoodItemsResponse;
import com.foodorder.userservice.dtos.UserRequestDto;
import com.foodorder.userservice.dtos.UserResponseDto;
import com.foodorder.userservice.dtos.WalletRequestDto;
import com.foodorder.userservice.exceptions.FoodItemNotFoundException;

public interface UserService extends UserDetailsService {

	UserResponseDto addUser(@Valid UserRequestDto userRequestDto);

	String findById(int userId, String userRole, Principal principal);

	List<FoodItemsResponse> findFoodItemByName(@NotNull String foodName, int page, int size)
			throws FoodItemNotFoundException;

	String addAmountToUserWallet(@Valid WalletRequestDto walletResquest);

	// OrderResponse placeOrder(OrderRequestDto orderRequestDto)throws
	// OrderNotPlacedException, FoodItemNotFoundException;

}
