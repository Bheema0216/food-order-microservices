package com.foodorder.userservice.constants;

public class UserConstants {
	public static final String USER_IS_ALREADY_EXISTED_EXCEPTION_MESSAGE = "User is already existed with given email or phone number";

	public static final String DEFAULT_ROLE = "ROLE_USER";
	public static final String[] ADMIN_ACCESS = { "ROLE_ADMIN", "ROLE_MODERATOR" };
	public static final String[] MODERATOR_ACCESS = { "ROLE_MODERATOR" };

	public static final String FOOD_ITEM_NOT_FOUND = "No Food items are available with your seacrh food-name : ";

	public static final String WALLET_SERVICE_FALLBACK_METHOD_RESPONSE = "Wallet-Service is currently down, please try after sometime";

	public static final String USER_NOT_FOUND = "No User found with given user id : ";

	public static final String[] SECURITY_ANT_MATCHERS = { "/users", "/users/foodmenus", "/users/wallets",
			"/users/orders" };

	public static final String ORDER_NOT_PLACED = "No sufficient balance available in wallet, please add the amount to your wallet";
	public static final String PLACE_ORDER_FALLBACK_METHOD_RESPONSE = "Order-Service is currently down, please try after sometime";
}
