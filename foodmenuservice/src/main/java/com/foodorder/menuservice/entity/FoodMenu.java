package com.foodorder.menuservice.entity;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity
public class FoodMenu {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer foodId;

	private String foodName;
	
	//private int quantity;

	private double price;
	
	private String foodDescription;
	
	@Enumerated(EnumType.STRING)
	private FoodCategory foodCategory;
}
