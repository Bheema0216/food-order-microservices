package com.foodorder.menuservice.entity;

import lombok.Getter;

@Getter
public enum FoodCategory {

	VEG, NON_VEG;
}
