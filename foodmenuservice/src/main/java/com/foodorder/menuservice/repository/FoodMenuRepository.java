package com.foodorder.menuservice.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.foodorder.menuservice.entity.FoodMenu;

@Repository
public interface FoodMenuRepository extends JpaRepository<FoodMenu, Integer> {


	FoodMenu findByFoodName(String foodName);

	@Query("select f from FoodMenu f where f.foodName like %?#{escape([0])}% escape ?#{escapeCharacter()}")
	Page<FoodMenu> findContainingEscaped(String foodName,Pageable pageable);
	
}
