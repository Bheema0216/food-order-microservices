package com.foodorder.menuservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FoodmenuserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(FoodmenuserviceApplication.class, args);
	}

}
