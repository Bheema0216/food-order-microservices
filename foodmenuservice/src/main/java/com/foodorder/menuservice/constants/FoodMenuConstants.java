package com.foodorder.menuservice.constants;

public class FoodMenuConstants {

	public static final String FOOD_ITEM_ALREADY_EXISTS = "The Food item is already existed with the given food-name : ";
	public static final String FOOD_ITEM_NOT_FOUND = "No Food items are available with your seacrh food-name : ";
}
