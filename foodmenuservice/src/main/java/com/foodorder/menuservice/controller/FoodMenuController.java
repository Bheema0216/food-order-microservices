package com.foodorder.menuservice.controller;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.foodorder.menuservice.dtos.FoodItemsResponse;
import com.foodorder.menuservice.dtos.FoodMenuDto;
import com.foodorder.menuservice.dtos.MenuResponse;
import com.foodorder.menuservice.exceptions.FoodItemAlreadyExists;
import com.foodorder.menuservice.exceptions.FoodItemNotFoundException;
import com.foodorder.menuservice.service.FoodMenuService;

@RestController
@Validated
public class FoodMenuController {

	@Autowired
	FoodMenuService foodMenuService;

	@PostMapping("/foodmenus")
	public ResponseEntity<MenuResponse> addNewFoodItem(@Valid @RequestBody FoodMenuDto foodMenuDto) throws FoodItemAlreadyExists {
		MenuResponse response = foodMenuService.addNewFoodItem(foodMenuDto);
		return new ResponseEntity<>(response, HttpStatus.CREATED);
	}
	
	@GetMapping("/foodmenus/bynamelike")
	public ResponseEntity<List<FoodItemsResponse>> findFoodItemByNameLike(@NotNull @RequestParam String foodName, @RequestParam(defaultValue = "0") int page,
	        @RequestParam(defaultValue = "3") int size) throws FoodItemNotFoundException {
		System.out.println("food-service-nameLike method-controller");
		List<FoodItemsResponse> foodItemsResponseList = foodMenuService.findFoodItemByNameLike(foodName, page, size);
		
		return new ResponseEntity<>(foodItemsResponseList, HttpStatus.OK);
	}
	
	
	@GetMapping("/foodmenus")
	public ResponseEntity<FoodItemsResponse> findFoodItemByName(@NotNull @RequestParam String foodName) throws FoodItemNotFoundException, FoodItemAlreadyExists {
		FoodItemsResponse foodItemsResponse = foodMenuService.findFoodItemByName(foodName);
		return new ResponseEntity<>(foodItemsResponse, HttpStatus.OK);
	}
}
