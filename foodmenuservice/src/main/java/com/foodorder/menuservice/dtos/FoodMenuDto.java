package com.foodorder.menuservice.dtos;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import com.foodorder.menuservice.entity.FoodCategory;

import lombok.Data;

@Data
public class FoodMenuDto {
	
	private String foodName;

	//private int quantity;

	private double price;

	private String foodDescription;

	@Enumerated(EnumType.STRING)
	private FoodCategory foodCategory;
}
