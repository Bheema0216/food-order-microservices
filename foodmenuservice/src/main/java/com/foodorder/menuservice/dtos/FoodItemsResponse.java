package com.foodorder.menuservice.dtos;

import com.foodorder.menuservice.entity.FoodCategory;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FoodItemsResponse {
	private String foodName;

	private String foodDescription;

	private FoodCategory foodCategory;
	
	private double price;
}
