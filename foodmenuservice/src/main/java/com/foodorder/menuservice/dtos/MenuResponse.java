package com.foodorder.menuservice.dtos;

import com.foodorder.menuservice.entity.FoodCategory;

import lombok.Data;
@Data
public class MenuResponse {

	private Integer foodId;

	private String foodName;

	private String foodDescription;

	private FoodCategory foodCategory;
}
