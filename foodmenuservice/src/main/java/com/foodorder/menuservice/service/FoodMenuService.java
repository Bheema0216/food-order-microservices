package com.foodorder.menuservice.service;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.foodorder.menuservice.dtos.FoodItemsResponse;
import com.foodorder.menuservice.dtos.FoodMenuDto;
import com.foodorder.menuservice.dtos.MenuResponse;
import com.foodorder.menuservice.exceptions.FoodItemAlreadyExists;
import com.foodorder.menuservice.exceptions.FoodItemNotFoundException;

public interface FoodMenuService {

	MenuResponse addNewFoodItem(@Valid FoodMenuDto foodMenuDto) throws FoodItemAlreadyExists;

	List<FoodItemsResponse> findFoodItemByNameLike(@NotNull String foodName, int page, int size)throws FoodItemNotFoundException;

	FoodItemsResponse findFoodItemByName(@NotNull String foodName) throws FoodItemNotFoundException, FoodItemAlreadyExists;

}
