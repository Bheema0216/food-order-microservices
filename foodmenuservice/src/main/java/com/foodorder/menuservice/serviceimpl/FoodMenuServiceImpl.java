package com.foodorder.menuservice.serviceimpl;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.foodorder.menuservice.constants.FoodMenuConstants;
import com.foodorder.menuservice.dtos.FoodItemsResponse;
import com.foodorder.menuservice.dtos.FoodMenuDto;
import com.foodorder.menuservice.dtos.MenuResponse;
import com.foodorder.menuservice.entity.FoodMenu;
import com.foodorder.menuservice.exceptions.FoodItemAlreadyExists;
import com.foodorder.menuservice.exceptions.FoodItemNotFoundException;
import com.foodorder.menuservice.repository.FoodMenuRepository;
import com.foodorder.menuservice.service.FoodMenuService;

@Service
public class FoodMenuServiceImpl implements FoodMenuService {

	@Autowired
	FoodMenuRepository foodMenuRepository;

	@Override
	public MenuResponse addNewFoodItem(@Valid FoodMenuDto foodMenuDto) throws FoodItemAlreadyExists {

		FoodMenu foodMenu = foodMenuRepository.findByFoodName(foodMenuDto.getFoodName());

		if (foodMenu != null)
			throw new FoodItemAlreadyExists(FoodMenuConstants.FOOD_ITEM_ALREADY_EXISTS + foodMenuDto.getFoodName());

		FoodMenu menu = new FoodMenu();
		BeanUtils.copyProperties(foodMenuDto, menu);
		FoodMenu savedFoodItem = foodMenuRepository.save(menu);

		MenuResponse menuResponse = new MenuResponse();
		BeanUtils.copyProperties(savedFoodItem, menuResponse);
		return menuResponse;
	}

	@Override
	public List<FoodItemsResponse> findFoodItemByNameLike(@NotNull String foodName, int page, int size)
			throws FoodItemNotFoundException {

		Pageable paging = PageRequest.of(page, size);
		Page<FoodMenu> foodMenuPage = foodMenuRepository.findContainingEscaped(foodName, paging);

		List<FoodMenu> foodMenuList = foodMenuPage.getContent();

		if (foodMenuList.isEmpty())
			throw new FoodItemNotFoundException(FoodMenuConstants.FOOD_ITEM_NOT_FOUND + foodName);

		List<FoodItemsResponse> responseList = new ArrayList<FoodItemsResponse>(foodMenuList.size());

		foodMenuList.forEach(list -> responseList.add(new FoodItemsResponse(list.getFoodName(),
				list.getFoodDescription(), list.getFoodCategory(), list.getPrice())));

		return responseList;
		/*
		 * if (foodMenuList.isEmpty()) throw new
		 * FoodItemNotFoundException(FoodMenuConstants.FOOD_ITEM_NOT_FOUND + foodName);
		 * 
		 * List<FoodItemsResponse> responseList = new
		 * ArrayList<FoodItemsResponse>(foodMenuList.size());
		 * 
		 * foodMenuList.forEach(list -> responseList .add(new
		 * FoodItemsResponse(list.getFoodName(), list.getFoodDescription(),
		 * list.getFoodCategory(),list.getPrice())));
		 * 
		 * return responseList;
		 */
	}

	@Override
	public FoodItemsResponse findFoodItemByName(@NotNull String foodName)
			throws FoodItemNotFoundException, FoodItemAlreadyExists {
		FoodMenu foodMenu = foodMenuRepository.findByFoodName(foodName);
		if (foodMenu == null)
			throw new FoodItemNotFoundException(FoodMenuConstants.FOOD_ITEM_NOT_FOUND + foodName);

		FoodItemsResponse response = new FoodItemsResponse();
		BeanUtils.copyProperties(foodMenu, response);
		return response;
	}

}
