package com.foodorder.menuservice.exceptions;

public class FoodItemAlreadyExists extends Exception {

	private static final long serialVersionUID = 1L;

	public FoodItemAlreadyExists(String msg) {
		super(msg);
	}
}
