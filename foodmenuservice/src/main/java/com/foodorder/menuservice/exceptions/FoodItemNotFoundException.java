package com.foodorder.menuservice.exceptions;

public class FoodItemNotFoundException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public FoodItemNotFoundException(String msg) {
		super(msg);
	}
}
