package com.foodorder.menuservice.exceptions;

import java.util.HashMap;
import java.util.Map;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CommonValidationResponse extends CommonErrorResponse {
	private Map<String, String> errors = new HashMap<String, String>();
}
