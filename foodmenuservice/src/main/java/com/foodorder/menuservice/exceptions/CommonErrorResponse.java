package com.foodorder.menuservice.exceptions;

import java.time.LocalDateTime;

import lombok.Data;
@Data
public class CommonErrorResponse {
	private String message;

	private int statusCode;

	private LocalDateTime timeStamp;
}
