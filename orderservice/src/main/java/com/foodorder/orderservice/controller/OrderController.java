package com.foodorder.orderservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.foodorder.orderservice.dtos.OrderHistoryRequest;
import com.foodorder.orderservice.dtos.OrderHistoryResponse;
import com.foodorder.orderservice.dtos.OrderRequestDto;
import com.foodorder.orderservice.dtos.OrderResponse;
import com.foodorder.orderservice.exceptions.FoodItemNotFoundException;
import com.foodorder.orderservice.exceptions.NoOrdersHistoryAvailableException;
import com.foodorder.orderservice.exceptions.OrderNotPlacedException;
import com.foodorder.orderservice.service.OrderService;

@RestController
public class OrderController {

	@Autowired
	private OrderService orderService;

	@PostMapping("/orders")
	public ResponseEntity<OrderResponse> placeOrder(@RequestBody OrderRequestDto orderRequestDto)
			throws OrderNotPlacedException, FoodItemNotFoundException {
		OrderResponse orderResponse = orderService.placeOrder(orderRequestDto);
		System.out.println("place order-controller");
		return new ResponseEntity<>(orderResponse, HttpStatus.CREATED);
	}

	@PutMapping("/orders")
	public ResponseEntity<List<OrderHistoryResponse>> getOrderHistory(@RequestBody OrderHistoryRequest request)
			throws NoOrdersHistoryAvailableException {
		List<OrderHistoryResponse> response = orderService.getOrderHistory(request);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

}
