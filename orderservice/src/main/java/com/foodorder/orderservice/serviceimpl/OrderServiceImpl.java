package com.foodorder.orderservice.serviceimpl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.foodorder.orderservice.clients.FoodMenuClient;
import com.foodorder.orderservice.clients.WalletClient;
import com.foodorder.orderservice.constants.OrderConstants;
import com.foodorder.orderservice.dtos.FoodItemsResponse;
import com.foodorder.orderservice.dtos.OrderDetailsRequestDto;
import com.foodorder.orderservice.dtos.OrderDetailsResponse;
import com.foodorder.orderservice.dtos.OrderHistoryRequest;
import com.foodorder.orderservice.dtos.OrderHistoryResponse;
import com.foodorder.orderservice.dtos.OrderRequestDto;
import com.foodorder.orderservice.dtos.OrderResponse;
import com.foodorder.orderservice.dtos.WalletRequestDto;
import com.foodorder.orderservice.dtos.WalletResponse;
import com.foodorder.orderservice.entity.Order;
import com.foodorder.orderservice.entity.OrderDetails;
import com.foodorder.orderservice.exceptions.FoodItemNotFoundException;
import com.foodorder.orderservice.exceptions.NoOrdersHistoryAvailableException;
import com.foodorder.orderservice.exceptions.OrderNotPlacedException;
import com.foodorder.orderservice.repository.OrderDetailsRepository;
import com.foodorder.orderservice.repository.OrderRepository;
import com.foodorder.orderservice.service.OrderService;

@Service
@Transactional
public class OrderServiceImpl implements OrderService {

	@Autowired
	private OrderRepository orderRepository;
	@Autowired
	private OrderDetailsRepository orderDetailsRepository;

	@Autowired
	private FoodMenuClient foodMenuClient;

	@Autowired
	WalletClient walletClient;

	@Override
	public OrderResponse placeOrder(OrderRequestDto orderRequestDto)
			throws OrderNotPlacedException, FoodItemNotFoundException {

		List<OrderDetailsRequestDto> orderDetailsRequestList = orderRequestDto.getOrderDetailsRequestDto();

		List<OrderDetails> orderDetailsList = new ArrayList<OrderDetails>();

		Order order = new Order();

		double totalPrice = 0;
		double foodItemsPrice;

		for (OrderDetailsRequestDto orderDetailsRequestDto : orderDetailsRequestList) {

			FoodItemsResponse foodItem = foodMenuClient.findFoodItemByName(orderDetailsRequestDto.getFoodName());
			
			
			if (foodItem == null)
				throw new FoodItemNotFoundException(
						OrderConstants.FOOD_ITEM_NOT_FOUND + orderDetailsRequestDto.getFoodName());
			if(foodItem.getFoodName().equals("fallback")) 
				throw new FoodItemNotFoundException("FoodMenu service is down, please try after sometime");
			foodItemsPrice = foodItem.getPrice() * orderDetailsRequestDto.getQuntity();
			OrderDetails orderDetails = new OrderDetails();
			orderDetails.setPrice(foodItemsPrice);
			orderDetails.setUserId(orderRequestDto.getUserId());
			orderDetails.setOrder(order);
			BeanUtils.copyProperties(orderDetailsRequestDto, orderDetails);
			orderDetailsList.add(orderDetails);

			totalPrice = totalPrice + foodItemsPrice;
		}

		order.setUserId(orderRequestDto.getUserId());
		order.setOrderDate(LocalDate.now());
		order.setTotalPrice(totalPrice);

		WalletResponse walletResponse = null;
		walletResponse = walletClient.getWalletBalance(orderRequestDto.getUserId());
		if (walletResponse.getBalance() < 0) {
			throw new OrderNotPlacedException("Wallet service down");
		}

		if (walletResponse.getBalance() < totalPrice) {
			throw new OrderNotPlacedException(OrderConstants.ORDER_NOT_PLACED);
		}
		Order savedOrder = orderRepository.save(order);
		orderDetailsRepository.saveAll(orderDetailsList);

		WalletRequestDto walletRequest = new WalletRequestDto();
		walletRequest.setUserId(orderRequestDto.getUserId());
		walletRequest.setBalance(walletResponse.getBalance() - totalPrice);
		WalletResponse updatedWalletResponse = walletClient.updateWalletBalance(walletRequest);

		OrderResponse orderResponse = new OrderResponse();
		orderResponse.setOrderedDate(savedOrder.getOrderDate());
		orderResponse.setAvailableWalletBalance(updatedWalletResponse.getBalance());
		BeanUtils.copyProperties(savedOrder, orderResponse);
		return orderResponse;
	}

	@Override
	public List<OrderHistoryResponse> getOrderHistory(OrderHistoryRequest request)
			throws NoOrdersHistoryAvailableException {

		Integer userId = request.getUserId();
		LocalDate fromDate = request.getFromDate();
		LocalDate toDate = request.getToDate();
		List<Order> orders = orderRepository.findByUserIdAndOrderDateBetween(userId, fromDate, toDate);
		if (orders.isEmpty())
			throw new NoOrdersHistoryAvailableException(OrderConstants.NO_ORDERS_AVAILABLE);

		List<OrderHistoryResponse> orderHistoryResponseList = new ArrayList<OrderHistoryResponse>(orders.size());

		for (Order order : orders) {

			List<OrderDetails> oderDetailsList = order.getOrderDetails();
			List<OrderDetailsResponse> orderDetailsResponseList = new ArrayList<OrderDetailsResponse>(
					oderDetailsList.size());

			for (OrderDetails orderDetails : oderDetailsList) {
				OrderDetailsResponse orderDetailsResponse = new OrderDetailsResponse();
				BeanUtils.copyProperties(orderDetails, orderDetailsResponse);

				orderDetailsResponseList.add(orderDetailsResponse);
			}
			OrderHistoryResponse orderHistoryResponse = new OrderHistoryResponse();
			orderHistoryResponse.setOrderId(order.getOrderId());
			orderHistoryResponse.setOrderedDate(order.getOrderDate());
			orderHistoryResponse.setTotalPrice(order.getTotalPrice());
			orderHistoryResponse.setOrderDetailsResponse(orderDetailsResponseList);

			orderHistoryResponseList.add(orderHistoryResponse);

		}

		return orderHistoryResponseList;
	}

}
