package com.foodorder.orderservice.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.foodorder.orderservice.dtos.WalletRequestDto;
import com.foodorder.orderservice.dtos.WalletResponse;
import com.foodorder.orderservice.exceptions.OrderNotPlacedException;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;

@FeignClient("WALLET-SERVICE")
@CircuitBreaker(name = "walletcircuitbreaker", fallbackMethod = "walletServiceFallBackMethod")
public interface WalletClient {

	@PostMapping("/wallets")
	public WalletResponse updateWalletBalance(@RequestBody WalletRequestDto walletRequest);

	@GetMapping("/wallets")
	public WalletResponse getWalletBalance(@RequestParam Integer userId);

	public default WalletResponse walletServiceFallBackMethod(Exception ex) throws OrderNotPlacedException {
		WalletResponse walletResponse = new WalletResponse();
		walletResponse.setBalance(-1.0);
		return walletResponse;
	}
}
