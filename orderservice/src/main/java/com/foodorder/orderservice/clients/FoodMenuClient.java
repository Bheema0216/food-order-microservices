package com.foodorder.orderservice.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.foodorder.orderservice.constants.OrderConstants;
import com.foodorder.orderservice.dtos.FoodCategory;
import com.foodorder.orderservice.dtos.FoodItemsResponse;
import com.foodorder.orderservice.exceptions.FoodItemNotFoundException;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;

@FeignClient("FOODMENU-SERVICE")
@CircuitBreaker(name = "foodcircuitbreaker", fallbackMethod = "findFoodItemByNameFallBackMethod")
public interface FoodMenuClient {
	@GetMapping("/foodmenus")
	public FoodItemsResponse findFoodItemByName(@RequestParam String foodName);

	public default FoodItemsResponse findFoodItemByNameFallBackMethod(Exception ex) throws FoodItemNotFoundException {
		FoodItemsResponse response = new FoodItemsResponse("fallback", "soft", FoodCategory.VEG, 10.00);
		if (ex.getMessage().contains("No Food items")) {
			throw new FoodItemNotFoundException(OrderConstants.FOOD_ITEM_NOT_FOUND);
		}
		return response;
	}
}
