package com.foodorder.orderservice.dtos;

import java.time.LocalDate;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderHistoryResponse {
	private Integer orderId;

	private LocalDate orderedDate;

	private double totalPrice;
	
	private List<OrderDetailsResponse> orderDetailsResponse;
}
