package com.foodorder.orderservice.dtos;

import java.time.LocalDate;

import lombok.Data;

@Data
public class OrderHistoryRequest {
	private Integer userId;
	private LocalDate fromDate;
	private LocalDate toDate;
}
