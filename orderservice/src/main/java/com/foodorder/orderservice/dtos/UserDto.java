package com.foodorder.orderservice.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserDto {
	private Integer userId;
	private String userName;
	private String userPhoneNumber;
	private String email;
}
