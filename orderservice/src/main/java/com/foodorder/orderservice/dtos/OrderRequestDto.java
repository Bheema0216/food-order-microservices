package com.foodorder.orderservice.dtos;

import java.util.List;

import lombok.Data;

@Data
public class OrderRequestDto {

	private Integer userId;

	List<OrderDetailsRequestDto> orderDetailsRequestDto;

}
