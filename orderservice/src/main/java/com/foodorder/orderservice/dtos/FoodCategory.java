package com.foodorder.orderservice.dtos;

import lombok.Getter;

@Getter
public enum FoodCategory {
	VEG, NON_VEG;
}
