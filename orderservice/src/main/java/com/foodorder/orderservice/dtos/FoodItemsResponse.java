package com.foodorder.orderservice.dtos;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FoodItemsResponse {
	private String foodName;

	private String foodDescription;

	private FoodCategory foodCategory;

	private double price;
}
