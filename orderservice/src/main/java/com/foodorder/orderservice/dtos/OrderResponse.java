package com.foodorder.orderservice.dtos;

import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderResponse {
	private Integer orderId;

	private LocalDate orderedDate;

	private double totalPrice;
	private double availableWalletBalance;
}
