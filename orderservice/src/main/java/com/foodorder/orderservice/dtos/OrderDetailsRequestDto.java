package com.foodorder.orderservice.dtos;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderDetailsRequestDto {
	
	@NotNull
	@Size(min = 2,max = 20, message = "Food name length in between 2 to 20 chars")
	private String foodName;
	@NotNull
	private int quntity;
}
