package com.foodorder.orderservice.dtos;

import java.time.LocalDate;

import lombok.Data;

@Data
public class OrderDto {
	
	private Integer userId;

	private LocalDate orderDate;

	private double totalPrice;
}
