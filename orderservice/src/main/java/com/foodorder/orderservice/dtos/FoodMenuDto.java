package com.foodorder.orderservice.dtos;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FoodMenuDto {

	private Integer foodId;

	private String foodName;
	
	private double price;
	
	private String foodDescription;
	
	@Enumerated(EnumType.STRING)
	private FoodCategory foodCategory;
}
