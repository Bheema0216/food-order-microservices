package com.foodorder.orderservice.dtos;

import lombok.Data;

@Data
public class OrderDetailsResponse {

	private String foodName;
	
	private double price;

	private int quntity;
	
}
