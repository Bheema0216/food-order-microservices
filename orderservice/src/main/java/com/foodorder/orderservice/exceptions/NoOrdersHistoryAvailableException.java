package com.foodorder.orderservice.exceptions;

public class NoOrdersHistoryAvailableException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NoOrdersHistoryAvailableException(String msg) {
		super(msg);
	}
}
