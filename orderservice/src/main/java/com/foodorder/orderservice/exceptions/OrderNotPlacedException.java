package com.foodorder.orderservice.exceptions;

public class OrderNotPlacedException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public OrderNotPlacedException(String msg) {
		super(msg);
	}
}
