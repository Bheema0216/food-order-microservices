package com.foodorder.orderservice.exceptions;

import java.time.LocalDateTime;
import java.util.List;

import javax.validation.ConstraintViolationException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalExceptionHandler {
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<CommonValidationResponse> handleMethodArgumentNotValid(MethodArgumentNotValidException ex) {
		List<FieldError> errors = ex.getFieldErrors();
		CommonValidationResponse commonValidationResponse = new CommonValidationResponse();
		commonValidationResponse.setStatusCode(HttpStatus.BAD_REQUEST.value());
		commonValidationResponse.setTimeStamp(LocalDateTime.now());
		commonValidationResponse.setMessage("Please provide a valid input");

		for (FieldError error : errors) {
			commonValidationResponse.getErrors().put(error.getField(), error.getDefaultMessage());
		}
		return new ResponseEntity<>(commonValidationResponse, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(ConstraintViolationException.class)
	public ResponseEntity<CommonValidationResponse> handleConstraintViolationException(
			ConstraintViolationException ex) {
		CommonValidationResponse commonValidationResponse = new CommonValidationResponse();
		commonValidationResponse.setStatusCode(HttpStatus.BAD_REQUEST.value());
		commonValidationResponse.setTimeStamp(LocalDateTime.now());
		commonValidationResponse.setMessage("Please provide a valid input");
		ex.getConstraintViolations().forEach(error -> {
			commonValidationResponse.getErrors().put(error.getPropertyPath().toString(), error.getMessage());
		});
		return new ResponseEntity<>(commonValidationResponse, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(value = FoodItemNotFoundException.class)
	public final ResponseEntity<String> handlingFoodItemNotFoundException(FoodItemNotFoundException exception) {
		return new ResponseEntity<>(exception.getMessage(), HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler(value = OrderNotPlacedException.class)
	public final ResponseEntity<String> handlingOrderNotPlacedException(OrderNotPlacedException exception) {
		return new ResponseEntity<>(exception.getMessage(), HttpStatus.PAYMENT_REQUIRED);
	}

	@ExceptionHandler(value = NoOrdersHistoryAvailableException.class)
	public final ResponseEntity<String> handlingNoOrdersHistoryAvailableException(
			NoOrdersHistoryAvailableException exception) {
		return new ResponseEntity<>(exception.getMessage(), HttpStatus.NOT_FOUND);
	}
}
