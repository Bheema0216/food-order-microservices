package com.foodorder.orderservice.exceptions;

import org.springframework.stereotype.Component;

import com.foodorder.orderservice.constants.OrderConstants;

import feign.Response;
import feign.codec.ErrorDecoder;

@Component
public class AppFeignErrorDecoder implements ErrorDecoder {

	private final ErrorDecoder defaultErrorDecoder = new Default();

	@Override
	public Exception decode(String methodKey, Response response) {
		if (response.status() == 402) {
			throw new OrderNotPlacedException(OrderConstants.ORDER_NOT_PLACED);
		}
		if (response.status() == 404) {
			throw new FoodItemNotFoundException(OrderConstants.FOOD_ITEM_NOT_FOUND);
		}
		return defaultErrorDecoder.decode(methodKey, response);
	}

}
