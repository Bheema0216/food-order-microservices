package com.foodorder.orderservice.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.foodorder.orderservice.entity.Order;
@Repository
public interface OrderRepository extends JpaRepository<Order, Integer> {

	List<Order> findByUserIdAndOrderDateBetween(Integer userId, LocalDate fromDate, LocalDate toDate);

}
