package com.foodorder.orderservice.service;

import java.util.List;

import com.foodorder.orderservice.dtos.OrderHistoryRequest;
import com.foodorder.orderservice.dtos.OrderHistoryResponse;
import com.foodorder.orderservice.dtos.OrderRequestDto;
import com.foodorder.orderservice.dtos.OrderResponse;
import com.foodorder.orderservice.exceptions.FoodItemNotFoundException;
import com.foodorder.orderservice.exceptions.NoOrdersHistoryAvailableException;
import com.foodorder.orderservice.exceptions.OrderNotPlacedException;

public interface OrderService {

	OrderResponse placeOrder(OrderRequestDto orderRequestDto) throws OrderNotPlacedException,FoodItemNotFoundException;

	List<OrderHistoryResponse> getOrderHistory(OrderHistoryRequest request) throws NoOrdersHistoryAvailableException;

}
