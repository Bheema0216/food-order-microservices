package com.foodorder.orderservice.constants;

public class OrderConstants {
	public static final String FOOD_ITEM_NOT_FOUND = "No Food items are available with your seacrh food-name";

	public static final String ORDER_NOT_PLACED = "No sufficient balance available in wallet, please add the amount to your wallet";

	public static final String NO_ORDERS_AVAILABLE = "No order history found for given user and date range";

	public static final String WALLET_SERVICE_FALLBACK_METHOD_RESPONSE = "Wallet-Service is currently down, please try after sometime";
}
