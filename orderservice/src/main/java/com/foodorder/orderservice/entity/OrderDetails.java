package com.foodorder.orderservice.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
@Entity
public class OrderDetails {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	private Integer userId;
	
	private String foodName;
	
	private int quntity;
	
	private double price;
	@JsonIgnore
	@JsonBackReference
	@ManyToOne
	@JoinColumn(name = "order_id", referencedColumnName = "orderId")
	private Order order;
	
	
}
