package com.foodorder.orderservice.entity;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.Data;

@Data
@Entity
@Table(name = "orders")
public class Order {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer orderId;

	private int userId;

	private LocalDate orderDate;

	private double totalPrice;
	@JsonManagedReference
	@OneToMany(mappedBy = "order", fetch = FetchType.LAZY)
	private List<OrderDetails> orderDetails;

}
