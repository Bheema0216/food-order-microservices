package com.foodorder.walletservice.exceptions;

import java.util.HashMap;
import java.util.Map;

import lombok.Getter;

@Getter
public class CommonValidationResponse extends CommonErrorResponse {
	private Map<String, String> errors = new HashMap<String, String>();

}
