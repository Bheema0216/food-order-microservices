package com.foodorder.walletservice.exceptions;

public class UserWalletNotExistedException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UserWalletNotExistedException(String msg) {
		super(msg);
	}
}
