package com.foodorder.walletservice.constants;

public class WalletConstants {

	public static final String USER_NOT_EXISTS = "User wallet is not existed in database with given user id: ";

	public static final String ADD_AMT_TO_WALLET_RESPONSE = "Amount added to your wallet successfully and available wallet balance is Rs.";
}
