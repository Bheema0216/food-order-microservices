package com.foodorder.walletservice.service;

import javax.validation.Valid;

import com.foodorder.walletservice.dtos.WalletRequestDto;
import com.foodorder.walletservice.dtos.WalletResponse;

public interface WalletService {

	WalletResponse updateWalletBalance(@Valid WalletRequestDto walletRequest);

	WalletResponse getWalletBalance(Integer userId);

	String addAmountToWallet(@Valid WalletRequestDto walletRequest);

}
