package com.foodorder.walletservice.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.foodorder.walletservice.dtos.WalletRequestDto;
import com.foodorder.walletservice.dtos.WalletResponse;
import com.foodorder.walletservice.service.WalletService;

@RestController
public class WalletController {

	@Autowired
	private WalletService walletService;

	@PostMapping("/wallets")
	public ResponseEntity<WalletResponse> createWallet(@Valid @RequestBody WalletRequestDto walletRequest) {
		WalletResponse walletResponse = walletService.updateWalletBalance(walletRequest);
		return new ResponseEntity<>(walletResponse, HttpStatus.OK);
	}

	@GetMapping("/wallets")
	public ResponseEntity<WalletResponse> getWalletBalance(@RequestParam Integer userId) {
		WalletResponse walletResponse = walletService.getWalletBalance(userId);
		return new ResponseEntity<>(walletResponse, HttpStatus.OK);
	}

	@PutMapping("/wallets")
	public ResponseEntity<String> addAmountToWallet(@Valid @RequestBody WalletRequestDto walletRequest) {
		String response = walletService.addAmountToWallet(walletRequest);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
}
