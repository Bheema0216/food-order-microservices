package com.foodorder.walletservice.serviceimpl;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.foodorder.walletservice.constants.WalletConstants;
import com.foodorder.walletservice.dtos.WalletRequestDto;
import com.foodorder.walletservice.dtos.WalletResponse;
import com.foodorder.walletservice.entity.Wallet;
import com.foodorder.walletservice.exceptions.UserWalletNotExistedException;
import com.foodorder.walletservice.repository.WalletRepository;
import com.foodorder.walletservice.service.WalletService;

@Service
public class WalletServiceImpl implements WalletService {
	
	Logger log = org.slf4j.LoggerFactory.getLogger(getClass());

	@Autowired
	WalletRepository walletRepository;

	@Override
	public WalletResponse updateWalletBalance(@Valid WalletRequestDto walletRequest) {

		Wallet wallet = walletRepository.findByUserId(walletRequest.getUserId());
		if (wallet == null) {
			Wallet newWallet = new Wallet();
			BeanUtils.copyProperties(walletRequest, newWallet);
			Wallet saved = walletRepository.save(newWallet);
			WalletResponse walletResponse = new WalletResponse();
			BeanUtils.copyProperties(saved, walletResponse);
			return walletResponse;
		}
		BeanUtils.copyProperties(walletRequest, wallet);
		Wallet saved = walletRepository.save(wallet);
		WalletResponse walletResponse = new WalletResponse();
		BeanUtils.copyProperties(saved, walletResponse);
		return walletResponse;
	}

	@Override
	public WalletResponse getWalletBalance(Integer userId) {

		Wallet wallet = walletRepository.findByUserId(userId);

		if (wallet == null) {
			throw new UserWalletNotExistedException(WalletConstants.USER_NOT_EXISTS + userId);
		}
		WalletResponse walletResponse = new WalletResponse();
		BeanUtils.copyProperties(wallet, walletResponse);
		return walletResponse;
	}

	@Override
	public String addAmountToWallet(@Valid WalletRequestDto walletRequest) {
		Wallet wallet = walletRepository.findByUserId(walletRequest.getUserId());
		
		log.info("add wallet {}",walletRequest.getUserId());

		if (wallet == null) {
			throw new UserWalletNotExistedException(WalletConstants.USER_NOT_EXISTS + walletRequest.getUserId());
		}

		wallet.setBalance(walletRequest.getBalance() + wallet.getBalance());
		Wallet savedWallet = walletRepository.save(wallet);

		return WalletConstants.ADD_AMT_TO_WALLET_RESPONSE + savedWallet.getBalance();
	}

}
