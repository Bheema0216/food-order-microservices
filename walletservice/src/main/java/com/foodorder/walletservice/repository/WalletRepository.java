package com.foodorder.walletservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.foodorder.walletservice.entity.Wallet;
@Repository
public interface WalletRepository extends JpaRepository<Wallet, Integer> {

	Wallet findByUserId(Integer userId);

}
